To clone and configure all projects at once just invoke from this directory: 
```
$ ./gradlew cloneRepos
```
This gradle task clones all required repositories for project, preserving correct directory structure.

It is possible to verify if all repos are cloned properly:
```
$ ./gradlew verifyLocal
```

When something is missing, one can synchronize using the following task
``` 
$ ./gradlew cloneSync
```


If you need to change branch for multiple repositories at once, it can be done in the following way:
```
$ ./gradlew changeBranch --name=my_branch_name
```                                           


If you need to update gradle version(version is taken from BOM) for multiple repositories at once,  
it can be done in the following way:
```
$ ./gradlew changeGradleWrapperVersion
```

// TOOD:
- describe common.build configuration elements
- describe how to use templates
