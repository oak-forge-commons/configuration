package oak.forge.commons.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

class ChangeGradleWrapperVersion extends DefaultTask {

    private String repoDir
    private List<Repo> inputRepos
    private String osCmd
    private String gradleVersion

    @Input
    String getGradleVersion() {
        return gradleVersion
    }

    void setGradleVersion(String gradleVersion) {
        this.gradleVersion = gradleVersion
    }

    @Input
    String getOsCmd() {
        return osCmd
    }

    void setOsCmd(String osCmd) {
        this.osCmd = osCmd
    }

    @Input
    String getRepoDir() {
        return repoDir
    }

    void setRepoDir(String repoDir) {
        this.repoDir = repoDir
    }

    @Input
    List<Repo> getInputRepos() {
        return inputRepos
    }

    void setInputRepos(List<Repo> inputRepos) {
        this.inputRepos = inputRepos
    }

    @TaskAction
    void changeGradleWrapperVersion() {
        inputRepos.each {
            repo -> repo.changeGradleWrapperVersion(repoDir, osCmd, gradleVersion)
        }
    }
}
