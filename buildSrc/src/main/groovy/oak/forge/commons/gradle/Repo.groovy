package oak.forge.commons.gradle

import java.nio.file.Paths

class Repo {
	private static final boolean IS_DBG = false

	String url
	String localPath

	private Repo(String url, String localPath) {
		this.url = url
		this.localPath = localPath
	}

	static of(String url, String localPath) {
		return new Repo(url, localPath)
	}

	void verifyLocalPath(String targetDir) {
		def path = Paths.get(targetDir, localPath)
		if (!path.toFile().exists()) {
			println "local path not found: ${path}"
		}
	}

	void clone(String targetDir) {
		def path = Paths.get(targetDir, localPath)
		String cmd = "git clone ${url} ${path}"
		execute(cmd)
	}

	void cloneSync(String targetDir) {
		def path = Paths.get(targetDir, localPath)
		if (!path.toFile().exists()) {
			print "local path not found: ${path} => "
			String cmd = "git clone ${url} ${path}"
			execute(cmd)
		}
	}

	void changeBranch(String targetDir, String branchName) {
		def path = Paths.get(targetDir, localPath)
		String cmd = "git checkout ${branchName}"
		execute(cmd, path.toFile())
	}

	void changeGradleWrapperVersion(String targetDir, String osCmd, String gradleVersion) {
		def path = Paths.get(targetDir, localPath)
		String cmd = "${osCmd}gradlew wrapper --gradle-version ${gradleVersion} --distribution-type all"
		execute(cmd, path.toFile())
	}

	private static String cmdVariant(String cmd) {
		println cmd
		IS_DBG ? "echo '${cmd}'" : cmd
	}

	private static void execute(String cmd) {
		Process process = Runtime.getRuntime().exec(cmdVariant(cmd))
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))
		String line
		while ((line = reader.readLine()) != null) {
			System.out.println(line)
		}
		reader.close()
	}

	private static void execute(String cmd, File dir) {
		Process process = Runtime.getRuntime().exec(cmdVariant(cmd), null, dir)
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))
		String line
		while ((line = reader.readLine()) != null) {
			System.out.println(line)
		}
		reader.close()
	}
}
